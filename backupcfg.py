# Declaring jobs and the file paths they will use for the source and delivery - Dictionary of job definitions

jobs = {
    "job1": ['/home/ec2-user/environment/ictnwk409-class/data/dir1','/home/ec2-user/environment/ictnwk409-class/data/dir2'],
    "job2": ['/home/ec2-user/environment/ictnwk409-class/data/file1','/home/ec2-user/environment/ictnwk409-class/data/dir2'],
    "job3": ['/home/ec2-user/environment/backup/dir5','/home/ec2-user/environment/backup/dir6']
}

#Creating a logfile path
logfile = '/home/ec2-user/environment/ictnwk409-class/backup.log'

#Creating a usage_msg error message
usage_msg = "Usage: python backup.py <job_name>"

#Creating a job_msg error message
job_msg = "Incorrect Job Name: Job Name Unknown"

# Email credentials for the account to send error messages to
recipient  = 'example@email.com.au'
email_user = 'example@email.com.au'
email_pwd  = 'xxxxxxxxxx'
server     = 'mail.example.com'
port       = 587