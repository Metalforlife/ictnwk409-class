#!/usr/bin/python3

import os, sys, pathlib, shutil, smtplib # Importing sys, pathlib and shutil modules for use in the script
from datetime import datetime # Importing the datetime module for use
from backupcfg import jobs, logfile, usage_msg, job_msg, recipient, email_user, email_pwd, server, port # Importing details from the backupcfg.py file

message = [] # Declaring the quotation marks as messages to print to the screen
errors = 0 # Declaring errors as 0

now = datetime.now() # Declaring now as the datetime.now() string
datestring = now.strftime("%Y-%m-%d-%H:%M:%S") # Declaring datestring at the now.strftime() string

###############
# Functions
###############
# With the job list, use the do_logfile function
def do_logfile(job):
    
# Use the datestring, message and errors variables that have been set up outside the function    
    global datestring
    global message
    global errors
    
# Open  the log file, create a log message with the datetring, the job name and message as the layout and close the log file
    file = open(logfile,"a")
    for msg in message:
        logmsg = datestring + " " + job + " " + msg
        file.write(logmsg + "\n") 
    file.close()
    
# With the job list, use the do_backup function     
def do_backup(job):

# Use the message and errors variables that have been set up outside the function
    global message
    global errors
    
# Getting the source and destination paths
    dst = jobs[job][1]
    src = jobs[job][0]
    
# Test if the source path exists. If it fails, print an error message and add an integer to the errors 
    if not os.path.exists(src):
        message.append(src + " does not exist ")
        errors += 1 
# Test if the destination path exists. If it fails, print an error message and add an integer to the errors
    if not os.path.exists(dst):
        message.append(dst + " does not exist ")
        errors += 1 
        
# If there are no fails, continue
    if errors == 0:

# Testing if the source is a file or a directory
        p = pathlib.Path(src)
        is_a_file = p.is_file()
        is_a_dir = p.is_dir()

# Creating a path to copy the data between the source and destination with the file name as well as a time and date stamp
        path = pathlib.PurePath(src)
        dstpath = dst + "/" + path.name + "-" + datestring

# If it is a file, copy it with shutil following the source and destination paths or add an increment to the message and error sections 
        if is_a_file == True:
            try:
                shutil.copy2(src, dstpath)
                message.append("Backup job " + job + " SUCCESS")
            except:
                message.append("Backup job " + job + " FAIL")
                errors += 1 

# If it is a directory, copy it with shutil, while following the source and destination paths or add an increment to the message and error sections
        if is_a_dir == True:
            try:
                shutil.copytree(src, dstpath)
                message.append("Backup job " + job + " SUCCESS")
            except:
                message.append("Backup job " + job + " FAIL")
                errors += 1 
            
            # Declare the name of the backup file name to the source file path with the time and date and print it to the screen
            backup_dir_newname = src + "-" + datestring
            print(backup_dir_newname)
     
# Creates an email with the job name as the parameter, sends it to the declared email address and closes the email        
def do_email(job):

    global message

    header     = 'To: ' + recipient + '\n' + 'From: ' + email_user + '\n' + 'Subject: Backup Error ' + job + '\n'
    
    msg        = header + '\n'
    for item in message:
        msg = msg + item + '\n'
    msg = msg + ' \n\n'
    
    smtpserver = smtplib.SMTP(server, port)
    smtpserver.ehlo()
    smtpserver.starttls()
    smtpserver.login(email_user, email_pwd)
    smtpserver.sendmail(email_user, recipient, msg)
    smtpserver.quit()
    
################
# Script Logic
################

if len(sys.argv) == 2: # After python, the amount of command line arguments must equal 2
    job = sys.argv[1] # One of the jobs listed in the jobs declaration must be declared

# The job you are wanting to do must have a valid file path - Testing the path
    if job in jobs:
        do_backup(job)
        
# If the error count is more than 0, send an email alert
        if errors > 0:
            
            # Send an email if there is an error to the address that has been declared
            pass
            do_email(job) 
            
            
            
            
# Create a log whether it has succeded or failed 
        do_logfile(job)

# If the job name is not listed, print an error message
    else:    
        print(job_msg)

# Print an error message if a job name is not declared
else:
    print(usage_msg)
    